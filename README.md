# Example flow

This example flow demonstrates how the LibrEDA framework can be used. It is not complete nor does it target any real technology for now.

Currently the flow is very simple.

Inputs are:

* A directory with the layouts of the standard-cells in the OASIS format.
* A LEF file with the definitions of the standard-cells.
* A verilog file with the interface definitions of the standard-cells.
* A gate-level netlist of the circuit.

Output is a placed and routed layout.

## Rust

To run this example, a recent Rust toolchain needs to be installed.
Please follow the [official instructions](https://www.rust-lang.org/tools/install).

## Run the example flow

First clone the full LibrEDA framework.
The framework consists of multiple repositories that are bundled together in the 'workspace' repository.

```sh
git clone --recursive https://codeberg.org/LibrEDA/libreda-rs-workspace
cd libreda-rs-workspace/libreda-examples
```

Run some examples:
```sh

cd test

# A small layout.
# Completes within minutes. Most of the time will be spent for compiling the program.
./run_example_tiny.sh

# A small RISC-V core with ~5k cells.
# Takes ~15min to complete on a ThinkPad T420.
./run_example_5k.sh

# A RISC-V SoC with 2kB of RAM and ~110k logic cells.
# Takes 4 hours to complete on a ThinkPad T420.
./run_example_100k.sh
```

### Note about included test designs

The included designs are already synthesized for FreePDK45.

Their sources can be found here:
* [RV32 SoC 'Murax'](https://github.com/SpinalHDL/VexRiscv/blob/master/src/main/scala/vexriscv/demo/Murax.scala)
* [RV32E core 'ottocore'](https://codeberg.org/tok/attocore)

## Generate standard-cell layouts for the FreePDK45

[LibreCell](https://codeberg.org/tok/librecell) can be used to also create the layouts of standard-cells.
Of course, the layouts alone are not sufficient. Also timing information would be necessary at some point but that is out of the scope of this example.

To generate a set of standard-cell which can be used with this example place-and-route flow proceed as follows:

Install librecell:
```sh
pip install librecell=0.0.15
```

Generate the layouts:
```sh
cd ./generate-stdcell-library/layout
make
```

The design rules for the standard-cells are specified in the file `freepdk45.py` and the netlists are specified in `cells.sp`.

Now point the place-and-route tool to the newly generated layouts.

```sh
RUST_LOG=info cargo run \
    --bin libreda-example -- \
    --cell-layouts generate-stdcell-library/layout/ \
    --cell-library test/data/gscl45nm_interfaces.v \
    --lef test/data/gscl45nm.lef \
    --netlist test/data/comb_chip_45_nl.v \
    --top my_chip \
    --height 40000 --width 40000 \
    --output test/output/example_output.oas

```

