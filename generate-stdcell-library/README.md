# Generate standard-cell layouts

This example shows how to generate layouts of standard-cells based on 
the netlists from the FreePDK45.

Install librecell:
```sh
pip install librecell=0.0.15 # Later versions could also work.
```

Generate the layouts of some cells specified in the Makefile:
```sh
cd ./generate-stdcell-library/layout
make
```
