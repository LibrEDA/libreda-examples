/*
 * Copyright (c) 2020-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// stdlib
use std::borrow::Borrow;
use std::collections::{BTreeSet, HashMap, HashSet};
use std::fs;
use std::io::{BufReader, BufWriter};
use std::process::exit;
// crates.io
use itertools::Itertools;
use log::*;

use libreda_db::prelude as db;
use libreda_db::prelude::*;

use libreda_oasis::*;
use libreda_structural_verilog::*;

use libreda_pnr::util::netlist_validation;

use electron_placer::eplace_ms::EPlaceMS;
use electron_placer::SimpleQuadraticPlacer;
use libreda_db::layout::types::UInt;
use libreda_lefdef::lef_ast::Layer;
use libreda_lefdef::{LEFDesignRuleAdapter, LefDefParseError, LEF};
use libreda_pnr::design::SimpleDesignRef;
use libreda_pnr::place::mixed_size_placer::MixedSizePlacer;
use libreda_pnr::place::mixed_size_placer_cascade::PlacerCascade;
use libreda_pnr::place::placement_problem::PlacementStatus;
use libreda_pnr::place::stdcell_placer::PlacerAdapter;
use libreda_pnr::rebuffer::buffer_insertion::SimpleBufferInsertion;
use mycelium_router::power_router::*;
use std::ffi::OsStr;

use libreda_pnr::route::prelude::*;

use mycelium_router::maze_router::{SimpleGlobalRoute, SimpleGlobalRouter, SimpleMazeRouter};

use libreda_lefdef::def_ast::Tracks;
use libreda_lefdef::import::LEFImportOptions;
use mycelium_router::mycelium_detail_route_v2::create_routing_tracks;
use mycelium_router::mycelium_detail_route_v2::RoutingError;
use mycelium_router::pin_access_analysis::*;

/// Bundle of layout, netlist, technology data,
/// macro dimension, etc.
#[derive(Clone)]
pub struct SimpleFlow<C>
where
    C: db::L2NEdit,
{
    /// Layout and netlist data.
    pub chip: C,
    /// Path to technology LEF file.
    pub tech_lef_path: std::path::PathBuf,
    /// Technology LEF data.
    pub tech_lef: LEF,
    pub top_cell: Option<C::CellId>,
    /// Names of clock nets in the top cell.
    pub clock_nets: Vec<String>,
    /// Names of reset nets in the top cell.
    pub reset_nets: Vec<String>,
    /// The shape of the chip/macro.
    pub core_area: Option<db::SimplePolygon<C::Coord>>,
    /// The layer to be used to for cell outlines (abutment boxes).
    pub outline_layer: Option<C::LayerId>,
    /// Target density for placement. Must be a value in the range of `(0.0, 1.0]`.
    pub placement_target_density: f64,
    /// Maximum number of iterations for global placement.
    pub placement_max_iter: usize,
}

impl<C> SimpleFlow<C>
where
    C: db::L2NEdit<Coord = db::Coord> + L2NBaseMT + Default,
{
    pub fn new() -> Self {
        let mut simple_flow = Self {
            chip: Default::default(),
            tech_lef_path: Default::default(),
            tech_lef: Default::default(),
            top_cell: Default::default(),
            clock_nets: Default::default(),
            reset_nets: Default::default(),
            core_area: Default::default(),
            outline_layer: Default::default(),
            placement_target_density: 0.5,
            placement_max_iter: 2000,
        };

        simple_flow.init();

        simple_flow
    }

    fn init(&mut self) {
        self.chip.set_dbu(1000);
        // self.chip.set_dbu(2000);
        self.initialize_layers();
    }

    /// Get a read-only reference to the top cell.
    /// Panics if no top-cell is defined.
    fn top_cell_ref(&self) -> CellRef<C> {
        self.chip
            .cell_ref(self.top_cell.as_ref().expect("no top-cell defined"))
    }

    /// Register the name of a clock net.
    pub fn add_clock_net(&mut self, name: String) {
        self.clock_nets.push(name);
    }

    /// Register the name of a reset net.
    pub fn add_reset_net(&mut self, name: String) {
        self.reset_nets.push(name);
    }

    /// Read the technology LEF file.
    pub fn read_technology_lef(&mut self, path: &str) -> Result<(), LefDefParseError> {
        info!("Read technology LEF: {}", path);
        self.tech_lef_path = path.into();
        let mut lef_file = BufReader::new(fs::File::open(path).unwrap());
        let lef_result = libreda_lefdef::lef_parser::read_lef_bytes(&mut lef_file);
        if let Err(err) = &lef_result {
            error!("Failed to read LEF: {}", err)
        }
        self.tech_lef = lef_result?;

        Ok(())
    }

    pub fn report_routing_stack(&self) {
        {
            // Print statistics.
            let technology_lef = &self.tech_lef.technology;
            log::info!(
                "Number of routing layers: {}",
                technology_lef
                    .layers
                    .iter()
                    .filter(|l| match l {
                        Layer::Routing(_) => true,
                        _ => false,
                    })
                    .count()
            );
            log::info!(
                "Number of via layers: {}",
                technology_lef
                    .layers
                    .iter()
                    .filter(|l| match l {
                        Layer::Cut(_) => true,
                        _ => false,
                    })
                    .count()
            );
        }

        {
            // Get names of routing layer from the LEF file.
            let layer_stack_names = self.get_layer_stack_names();
            log::info!(
                "Routing layer stack: {}",
                layer_stack_names.iter().join(", ")
            );
        }
    }

    /// Get names of routing layers, ordered from lowest (close to silicon) to top.
    pub fn get_routing_layer_names(&self) -> Vec<String> {
        self.tech_lef
            .technology
            .layers
            .iter()
            .filter_map(|layer| match layer {
                Layer::Routing(l) => Some(l.name.clone().into()),
                _ => None,
            })
            .collect()
    }

    /// Get names of via layers, ordered from lowest (close to silicon) to top.
    pub fn get_via_layer_names(&self) -> Vec<String> {
        self.tech_lef
            .technology
            .layers
            .iter()
            .filter_map(|layer| match layer {
                Layer::Cut(l) => Some(l.name.clone().into()),
                _ => None,
            })
            .collect()
    }

    /// Get names of routing layer from the LEF file.
    pub fn get_layer_stack_names(&self) -> Vec<String> {
        self.tech_lef
            .technology
            .layers
            .iter()
            .filter_map(|layer| {
                match layer {
                    Layer::Routing(l) => Some(l.name.clone().into()),
                    Layer::Cut(l) => Some(l.name.clone().into()),
                    _ => None, // Ignore other layers than metal and via.
                }
            })
            .collect()
    }

    /// Get IDs of routing layers, ordered from lowest (close to silicon) to top.
    pub fn get_routing_layers(&mut self) -> Vec<C::LayerId> {
        // Create a list of layer IDs of the routing layers.
        let layer_name2index = self.get_layer_name_mapping();
        self.get_routing_layer_names()
            .iter()
            .filter_map(|name| layer_name2index.get(name).copied())
            .map(|(idx, dtype)| self.chip.find_or_create_layer(idx, dtype))
            .collect()
    }

    /// Get IDs of via layers, ordered from lowest (close to silicon) to top.
    pub fn get_via_layers(&mut self) -> Vec<C::LayerId> {
        // Create a list of layer IDs of the routing layers.
        let layer_name2index = self.get_layer_name_mapping();
        self.get_via_layer_names()
            .iter()
            .filter_map(|name| layer_name2index.get(name).copied())
            .map(|(idx, dtype)| self.chip.find_or_create_layer(idx, dtype))
            .collect()
    }

    /// Read netlist library of cells.
    // TODO: Pass error
    pub fn read_cell_netlist_library(&mut self, path: &str) {
        // Read library.
        info!("Reading netlist library: {}", path);

        // Load interfaces of standard-cells.
        let reader = StructuralVerilogReader::new().load_blackboxes(true); // Skip contents of cells. Only load the interfaces.

        // Load interfaces of standard-cells.
        reader
            .read_into_netlist(
                &mut BufReader::new(fs::File::open(path).unwrap()),
                &mut self.chip,
            )
            .expect("Failed to read netlist.");

        info!("Number library cells: {}", self.chip.each_cell().count());
    }

    // TODO: Pass error
    pub fn read_design_netlist(&mut self, path: &str) {
        info!("Read top netlist: {}", path);
        let reader = StructuralVerilogReader::new().load_blackboxes(false);

        reader
            .read_into_netlist(&mut fs::File::open(path).unwrap(), &mut self.chip)
            .expect("Failed to load netlist.");

        // Print some statistics:
        info!(
            "Number of top-level cells: {}",
            self.chip.each_top_level_cell().count()
        );
    }

    pub fn select_top_cell(&self, top_cell_name: &str) -> Result<C::CellId, ()> {
        let top = if let Some(top) = self.chip.cell_by_name(top_cell_name) {
            top
        } else {
            log::error!("Top circuit could not be found: {}", top_cell_name);
            // List all possible choices.
            let top_circuits = self
                .chip
                .each_cell()
                // .filter(|c| netlist.num_references(c) == 0) // Get top level cells.
                .filter(|c| self.chip.num_child_instances(c) > 0) // Get top level cells.
                .sorted_by_key(|c| self.chip.num_child_instances(c)) // Sort by size of the circuit.
                .rev()
                .map(|c| format!("'{}'", self.chip.cell_name(&c)))
                .join(", ");
            log::error!("Top circuit should be one of: {}", top_circuits);
            return Err(());
        };

        info!("Top level cell: {}", self.chip.cell_name(&top));

        Ok(top)
    }

    /// Deep-flatten the top with pads such that only leaf cells are inside the top cell.
    pub fn deep_flatten_design(&mut self, top: &C::CellId) {
        log::info!("Flatten the top such that it contains only leaf cells.");
        // let top = self.top_cell.clone().expect("No top cell selected.");
        loop {
            let instances = self.chip.each_cell_instance_vec(&top);
            let mut num_flattened_cells = 0;
            for inst in instances {
                let template = self.chip.template_cell(&inst); // Find the cell of this instance.
                let num_sub_instances = self.chip.num_child_instances(&template);
                if num_sub_instances > 0 {
                    // Flatten all non-leaf cells.
                    self.chip.flatten_circuit_instance(&inst);
                    num_flattened_cells += 1;
                }
            }
            if num_flattened_cells == 0 {
                // Nothing more to flatten.
                break;
            }
        }
    }

    /// Sanity check on netlist: Check that all nets in the top cell have a driver.
    pub fn validate_netlist_check_drivers(&self) {
        {
            // Do sanity checks on the netlist.
            let validation_result = netlist_validation::check_drivers_in_cell(
                &self.chip,
                &self.top_cell.as_ref().expect("No top-cell defined."),
            );
            if validation_result.is_err() {
                log::warn!("Some problems with the netlist such as missing tie-cells are about to be resolved.");
            }
        }
    }

    pub fn get_pad_positions(
        &self,
        pad_instances: &Vec<C::CellInstId>,
        (core_width, core_height): (db::Coord, db::Coord),
    ) -> HashMap<C::CellInstId, db::Point<C::Coord>> {
        // Create initial positions for the pads.

        let num_pads = pad_instances.len();
        let num_pads_per_side = ((num_pads + 3) / 4) as i32;

        // Distance from the pad ring to the core.
        let margin = 10000;

        let w = core_width + 2 * margin;
        let h = core_height + 2 * margin;

        // Distribute pad positions evenly around the rectangular core.
        let positions_a = (0..num_pads_per_side).map(|i| {
            db::Point::new(-margin, -margin) + db::Vector::new(w, 0) * i / num_pads_per_side
        });
        let positions_b = (0..num_pads_per_side).map(|i| {
            db::Point::new(core_width + margin, -margin)
                + db::Vector::new(0, h) * i / num_pads_per_side
        });
        let positions_c = (0..num_pads_per_side).map(|i| {
            db::Point::new(core_width + margin, core_height + margin)
                + db::Vector::new(-w, 0) * i / num_pads_per_side
        });
        let positions_d = (0..num_pads_per_side).map(|i| {
            db::Point::new(0 - margin, core_height + margin)
                + db::Vector::new(0, -h) * i / num_pads_per_side
        });

        let positions = positions_a
            .chain(positions_b)
            .chain(positions_c)
            .chain(positions_d);

        let positions: HashMap<_, _> = pad_instances.iter().cloned().zip(positions).collect();
        positions
    }

    /// Get names of cells types used in the top cell.
    pub fn get_used_std_cell_names(&self) -> Vec<String> {
        if let Some(top) = &self.top_cell {
            self.chip
                .each_cell_dependency(top)
                .map(|c| self.chip.cell_name(&c).to_string())
                .collect()
        } else {
            vec![]
        }
    }

    pub fn get_layer_name_mapping(&self) -> HashMap<String, (UInt, UInt)> {
        // Define layer names and the mapping to layer indices.
        let layer_name2index: HashMap<String, (UInt, UInt)> = vec![
            // ("poly".into(), (15, 0)),
            // ("contact".into(), (16, 0)),
            ("metal1".into(), (21, 0)),
            ("via1".into(), (22, 0)),
            ("metal2".into(), (23, 0)),
            ("via2".into(), (24, 0)),
            ("metal3".into(), (25, 0)),
            ("via3".into(), (26, 0)),
            ("metal4".into(), (27, 0)),
            ("via4".into(), (28, 0)),
            ("metal5".into(), (29, 0)),
            ("via5".into(), (205, 0)),
            ("metal6".into(), (204, 0)),
        ]
        .into_iter()
        .collect();
        layer_name2index
    }

    /// Create all named layers and set their names.
    fn initialize_layers(&mut self) {
        for (name, (index, datatype)) in self.get_layer_name_mapping() {
            let l = self.chip.find_or_create_layer(index, datatype);
            self.chip.set_layer_name(&l, Some(name.into()));
        }
    }

    /// Manually add the layout of the dummy pad.
    pub fn load_pad_cell_layouts(&self, layout_library: &mut C) {
        log::info!("Manually create the layout of a dummy pad.");
        let pin_shape_layer = layout_library.find_or_create_layer(21, 0);
        let pin_label_layer = layout_library.find_or_create_layer(21, 1);

        let outline_layer = self
            .outline_layer
            .as_ref()
            .expect("outline_layer is not specified.");

        let pad_def = vec![
            ("dummy_pad_input", "Out", db::Direction::Input),
            ("dummy_pad_output", "In", db::Direction::Output),
        ];

        for (pad_name, pin_name, direction) in pad_def {
            let dummy_pad_cell = layout_library.create_cell(pad_name.to_string().into());

            let pin =
                layout_library.create_pin(&dummy_pad_cell, pin_name.to_string().into(), direction);

            // Insert pin shape.
            let geometry: db::Geometry<_> = db::Rect::new((-1000, -1000), (1000, 1000)).into();
            let pin_shape =
                layout_library.insert_shape(&dummy_pad_cell, &pin_shape_layer, geometry.clone());

            // Insert pin label.
            layout_library.insert_shape(
                &dummy_pad_cell,
                &pin_label_layer,
                db::Text::new(pin_name.to_string(), (0, 0).into()).into(),
            );

            // Insert pin outline.
            dbg!(outline_layer);
            layout_library.insert_shape(&dummy_pad_cell, outline_layer, geometry);

            // Assign shape to logical pin.
            layout_library.set_pin_of_shape(&pin_shape, Some(pin.clone()));
        }
    }

    /// Import vias from LEF.
    pub fn load_via_cells(layout_library: &mut C, lef: &LEF) -> Vec<C::CellId> {
        info!("Import via cells from LEF.");
        let options = LEFImportOptions::default();
        let result = libreda_lefdef::import::import_lef_vias(&options, &lef, layout_library);

        match result {
            Err(e) => {
                log::warn!("Failed to import vias from LEF: {}", e);
                // TODO: Propagate error or abort?
                vec![]
            }
            Ok(new_cells) => new_cells,
        }
    }

    /// Import all OASIS layout files from a directory.
    /// Put the cell layouts in to the current design.
    pub fn load_cell_layout_library(&mut self, cells_layout_path: &str) {
        // Load cell layouts.
        let mut layout_library = C::default();

        let oasis_reader = libreda_oasis::OASISStreamReader::new();

        info!("Load cell layouts: {}", cells_layout_path);

        // Read all cells found in the directory.
        let dir_content = fs::read_dir(cells_layout_path).unwrap();

        for entry in dir_content {
            if let Ok(entry) = entry {
                let oas_path = entry.path();

                // Read only files with an '.oas' extension.
                let extension = OsStr::new("oas");
                if oas_path.extension() == Some(extension) {
                    log::debug!("Read: {}", oas_path.to_string_lossy());
                    oasis_reader
                        .read_layout(
                            &mut fs::File::open(oas_path.into_boxed_path()).unwrap(),
                            &mut layout_library,
                        )
                        .expect("Failed to read cell layout.");
                }
            }
        }

        // self.load_pad_cell_layouts(&mut layout_library);

        // Print some statistics.
        info!(
            "Number of library cell layouts: {}",
            layout_library.num_cells()
        );

        {
            // Check that all necessary cell layouts are present.
            // TODO: Also take into account cells that are inserted during place & route (buffers, tie-cells, etc.).
            let missing_layouts: Vec<_> = self
                .get_used_std_cell_names()
                .iter()
                .filter(|&circuit_name| layout_library.cell_by_name(circuit_name).is_none())
                .cloned()
                .collect();
            if missing_layouts.is_empty() {
                info!("All necessary layout cells are present.")
            } else {
                error!("Missing cell layouts: {}", missing_layouts.join(", "));
            }
            assert!(
                missing_layouts.is_empty(),
                "Some cell layouts are missing: {}",
                missing_layouts.join(", ")
            );
        }

        // // Create empty via cells.
        // for via_cell in &via_cells {
        //     let via_name = layout_library.cell_name(via_cell);
        //     self.chip.create_cell(via_name);
        // }

        // Copy the shapes of each library cell into the design.
        for source_cell in layout_library.each_cell() {
            let cell_name = layout_library.cell_name(&source_cell);
            let target_cell = self.chip.cell_by_name(cell_name.borrow());
            // Copy shapes only if a cell with the same name exists in the design.
            if let Some(target_cell) = target_cell {
                log::debug!("Copy shapes of cell '{}'", &cell_name);
                db::copy_shapes_all_layers(
                    &mut self.chip,
                    &target_cell,
                    &layout_library,
                    &source_cell,
                );
            }
        }

        // Load via cells.
        let _via_cells = Self::load_via_cells(&mut self.chip, &self.tech_lef);
        // info!("Number of library cell layouts (including via cells): {}", layout_library.num_cells());
    }

    /// Get the geometric shape of the core.
    pub fn core_area(&self) -> db::SimplePolygon<C::Coord> {
        self.core_area.clone().expect("No core shape defined.")
    }

    /// Extract pin locations from the cell layouts.
    pub fn detect_pins_in_layout(&mut self) {
        info!("Extract pin locations from the cell layouts.");

        let pin_shape_and_label_layers = vec![
            ((21, 0), (21, 1)), // metal1
            ((23, 0), (23, 1)), // metal2
        ];

        for cell in self.chip.each_cell_vec() {
            for (pin_shape_layer, pin_label_layer) in &pin_shape_and_label_layers {
                let pin_shape_layer = self
                    .chip
                    .find_or_create_layer(pin_shape_layer.0, pin_shape_layer.1);
                let pin_label_layer = self
                    .chip
                    .find_or_create_layer(pin_label_layer.0, pin_label_layer.1);

                libreda_pnr::util::pin_detection::assign_pin_shapes_from_text_labels(
                    &mut self.chip,
                    &cell,
                    &pin_shape_layer,
                    &pin_label_layer,
                );
            }
        }
    }

    /// Get the dimensions of the library cells (size of abutment box).
    pub fn get_cell_outlines(&self) -> HashMap<C::CellId, db::Rect<C::Coord>> {
        // Store cells that don't have an abutment box (outline) defined to later display a warning.
        let mut cells_without_bounding_box = BTreeSet::new();

        let outline_layer = self
            .outline_layer
            .as_ref()
            .expect("No outline layer defined.");

        // Find the size of the abutment box for every used cell.
        let cell_dimensions: HashMap<_, db::Rect<db::Coord>> = self
            .chip
            .each_cell()
            .filter_map(|cell| {
                // Find bounding box of 'outline' layer.

                let bbox = self.chip.bounding_box_per_layer(&cell, &outline_layer);

                if let Some(bbox) = bbox {
                    Some((cell, bbox))
                } else {
                    let name = self.chip.cell_name(&cell);
                    cells_without_bounding_box.insert(name);
                    None
                }
            })
            .collect();

        if !cells_without_bounding_box.is_empty() {
            let outline_layer = self.chip.layer_info(&outline_layer);
            let outline_layer = (outline_layer.index, outline_layer.datatype);
            warn!(
                "Some cells have no abutment box (layer {:?}): {}",
                outline_layer,
                cells_without_bounding_box.iter().join(", ")
            );
        }

        cell_dimensions
    }

    /// Check if the design can actually be placed in the given core area.
    pub fn validate_layout_density(&self) -> bool {
        // Compute total needed area of the cells.
        let top = self.top_cell.as_ref().expect("No top cell defined.");
        let cell_dimensions = self.get_cell_outlines();
        let total_cell_area: f64 = self
            .chip
            .each_cell_instance(top)
            .map(|inst| {
                let template = self.chip.template_cell(&inst);
                cell_dimensions
                    .get(&template)
                    .map(|r| {
                        let r: Rect<f64> = r.cast();
                        r.height() * r.width()
                    })
                    .unwrap_or(0.)
            })
            .sum();
        // Estimate resulting placement density.
        let core_shape_float: db::SimplePolygon<f64> = self.core_area().cast();

        let density = {
            let area2: f64 = core_shape_float.area_doubled_oriented();
            total_cell_area / (area2 / 2.)
        };

        info!("Estimated core density: {:0.4}", density);
        if density > 1.0 {
            error!("Density is larger than 1 ({:0.4}).", density);
            return false;
        }
        return true;
    }

    /// Create the power grid.
    pub fn draw_power_grid(
        &mut self,
        core_area: &db::SimplePolygon<db::Coord>,
        row_height: db::Coord,
    ) {
        let power_router = PowerRouter {
            stripe_definition: vec![
                PowerLayer {
                    layer: (21, 0),
                    orientation: StripeOrientation::Horizontal,
                    offset_gnd: 0,
                    offset_pwr: row_height,
                    stripe_width: 130,
                    step: row_height * 2,
                }
                .into(),
                PowerVia {
                    layer: (22, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }
                .into(),
                PowerLayer {
                    layer: (23, 0),
                    orientation: StripeOrientation::Vertical,
                    offset_gnd: 0,
                    offset_pwr: row_height,
                    stripe_width: 200,
                    step: row_height * 2,
                }
                .into(),
                PowerVia {
                    layer: (24, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }
                .into(),
                PowerLayer {
                    layer: (25, 0),
                    orientation: StripeOrientation::Horizontal,
                    offset_gnd: 0,
                    offset_pwr: 800,
                    stripe_width: 400,
                    step: 10000,
                }
                .into(),
                PowerVia {
                    layer: (26, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }
                .into(),
                PowerLayer {
                    layer: (27, 0),
                    orientation: StripeOrientation::Vertical,
                    offset_gnd: 0,
                    offset_pwr: 800,
                    stripe_width: 400,
                    step: 10000,
                }
                .into(),
            ],
        };

        // Create the power grid.
        power_router.draw_power_grid(
            &mut self.chip,
            self.top_cell.as_ref().expect("No top cell defined."),
            core_area,
        );
    }

    /// # Parameters
    /// * `is_initial_placement`: When `true` a quadratic placer will be used as to find the initial placement.
    /// When `false`, the current placement will be used as initial value for ePlace.
    pub fn place_global(
        &mut self,
        top_cell: &C::CellId,
        fixed_instances: &HashSet<C::CellInstId>,
        is_initial_placement: bool,
    ) {
        // ** GLOBAL PLACEMENT **

        // Create placement engine.
        let placer = {
            // Create quadratic placer for initial placement.
            let quadratic_placer = SimpleQuadraticPlacer::new().max_iter(200);

            // Create ePlace engine which relies on the result of the initial placement of the quadratic placer.
            let eplace = {
                let mut eplace = EPlaceMS::new(self.placement_target_density);
                eplace.max_iter(self.placement_max_iter);
                eplace
            };

            // Put the placers into series.
            let mut placers: Vec<Box<dyn MixedSizePlacer<C>>> = vec![];
            if is_initial_placement {
                placers.push(Box::new(PlacerAdapter::new(quadratic_placer)));
            }
            placers.push(Box::new(eplace));
            PlacerCascade::new(placers)
        };

        // Region where cells can be placed.
        let core_shape = db::SimpleRPolygon::try_new(self.core_area().points())
            .expect("Failed to convert core shape into a rectilinear polygon.");

        let mut net_weights = HashMap::new();

        // Ignore clock and reset nets for the global placement.
        for clock_name in &self.clock_nets {
            // Find net or net of a pin by name.
            let name = clock_name.as_str();
            let net = self.chip.net_by_name(&top_cell, name).or(self
                .chip
                .pin_by_name(&top_cell, name)
                .and_then(|pin| self.chip.net_of_pin(&pin)));

            if let Some(net) = net {
                log::info!(
                    "Ignore clock net for global placement: {:?}",
                    self.chip.net_name(&net)
                );
                net_weights.insert(net, 0.);
            } else {
                log::warn!("Clock net or pin not found: {}", clock_name);
            }
        }
        for reset_name in &self.reset_nets {
            // Find net or net of a pin by name.
            let name = reset_name.as_str();
            let net = self.chip.net_by_name(&top_cell, name).or(self
                .chip
                .pin_by_name(&top_cell, name)
                .and_then(|pin| self.chip.net_of_pin(&pin)));

            if let Some(net) = net {
                log::info!(
                    "Ignore reset net for global placement: {:?}",
                    self.chip.net_name(&net)
                );
                net_weights.insert(net, 0.);
            } else {
                log::warn!("Reset net or pin not found: {}", reset_name);
            }
        }

        // Initialize placement status flags.
        let mut placement_status = HashMap::new();
        for cell_inst in self.chip.each_cell_instance(&top_cell) {
            let status = if fixed_instances.contains(&cell_inst) {
                PlacementStatus::Fixed
            } else {
                PlacementStatus::Movable
            };
            placement_status.insert(cell_inst, status);
        }

        // Assemble the placement problem.
        let design = SimpleDesignRef {
            fused_layout_netlist: &self.chip,
            top_cell: top_cell.clone(),
            cell_outlines: &self.get_cell_outlines(),
            placement_region: &vec![core_shape.clone()],
            placement_status: &placement_status,
            net_weights: &net_weights,
            placement_location: &Default::default(),
        };

        // Do placement.
        let positions = placer.find_cell_positions_impl(&design);

        match positions {
            Ok(positions) => {
                // Update cell positions.
                for (cell_inst, pos) in positions {
                    self.chip.set_transform(&cell_inst, pos);
                }
            }
            Err(_err) => {
                log::error!("Global placement failed.");
                exit(1);
            }
        }

        {
            // Print HPLW.
            let hplw: f64 = libreda_pnr::metrics::wirelength_estimation::hpwl(&self.top_cell_ref());
            let hplw_microns = hplw / (self.chip.dbu() as f64);
            log::info!("HPLW: {:0.1} um", hplw_microns);
        }
    }

    pub fn insert_tie_cells(&mut self) {
        log::info!("Insert tie-cells.");

        let tie_cell_generator =
            libreda_pnr::util::tie_cell_insertion::SimpleTieCellInsertionEngine {
                tie_cell_low: "TIELOW".to_string(),
                tie_cell_high: "TIEHIGH".to_string(),
                max_fanout: 10,
                max_distance: 1000,
            };

        let top = self.top_cell.as_ref().expect("No top cell defined.");
        for net in vec![self.chip.net_zero(top), self.chip.net_one(top)] {
            log::info!(
                "Add tie-cells for net {:?}. ({} sinks)",
                self.chip.net_name(&net),
                self.chip.num_net_terminals(&net)
            );
            tie_cell_generator
                .insert_tie_cells_on_net(&mut self.chip, &net)
                .expect("Tie-cell insertion failed.");
        }
    }

    /// Insert buffer-trees on high-fanout nets.
    /// Return the created nets.
    pub fn rebuffer_high_fanout_nets(&mut self) -> Vec<C::NetId> {
        // Find high-fanout nets and insert buffer trees.
        log::info!("Insert buffer trees on high-fanout nets.");
        let top = self.top_cell.clone().expect("No top cell defined");

        let max_fanout = 4; // TODO: Centralize configuration.

        let high_fanout_nets: Vec<_> = self
            .chip
            .each_internal_net(&top)
            .filter(|n| self.chip.num_net_terminals(n) > max_fanout)
            .collect();

        log::info!(
            "Number of high-fanout nets in '{}' (with more than {} terminals): {}",
            self.chip.cell_name(&top),
            max_fanout,
            high_fanout_nets.len()
        );
        log::info!("Insert buffers into high-fanout nets.");

        let buffer_generator = arboreus_cts::simple_buffer_tree::SimpleBufferInsertionEngine {
            inverting_buffer_cell: "INVX1".into(),
            max_fanout: max_fanout as u32,
        };

        let mut buffer_tree_nets = vec![];
        for n in high_fanout_nets {
            log::debug!("Add buffer tree into net {:?}.", self.chip.net_name(&n));
            let (_buffer_insts, nets) = buffer_generator
                .add_buffer_tree_on_net(&mut self.chip, &n)
                .expect("Buffer insertion failed.");
            buffer_tree_nets.extend(nets);
            buffer_tree_nets.push(n);
        }
        buffer_tree_nets
    }

    pub fn route_with_mycelium(&mut self) {
        let top_cell = self.top_cell.clone().expect("No top cell defined.");

        let routing_layers = self.get_routing_layers();
        let via_layers = self.get_via_layers();

        // TODO: Get routing pitch from design rules or track definitions.
        let wire_width = 50i32;
        let min_spacing = 50;
        let routing_pitches = vec![wire_width + min_spacing; routing_layers.len()];

        // Get design rules from LEF.
        let design_rules = LEFDesignRuleAdapter::new(&self.tech_lef, &self.chip);

        // Get wire costs based on the routing direction.
        let wire_weights: Vec<(i32, i32)> = routing_layers
            .iter()
            .map(|layer| {
                let direction = design_rules
                    .preferred_routing_direction(layer)
                    .unwrap_or(db::Orientation2D::Horizontal);
                match direction {
                    Orientation2D::Horizontal => (2, 20),
                    Orientation2D::Vertical => (20, 2),
                }
            })
            .collect();

        assert!(
            wire_weights.len() <= routing_layers.len(),
            "Try to use more routing layers than specified in the technology."
        );

        let via_cost = 1000;

        let global_router = SimpleGlobalRouter::new(
            routing_pitches,
            wire_weights.clone(),
            via_cost,
            routing_layers.clone(),
            via_layers.clone(),
        );

        // Global routing
        let global_routes = {
            let global_routing_problem = {
                let top_cell = self.top_cell.clone().unwrap();
                let all_nets = self.chip.each_internal_net(&top_cell).collect();

                let mut routing_problem: SimpleRoutingProblem<_, ()> =
                    SimpleRoutingProblem::new(&self.chip, top_cell);
                routing_problem.nets = all_nets;

                routing_problem
            };

            let routes = global_router.route(&global_routing_problem);

            match routes {
                Err(unrouted_nets) => {
                    log::error!(
                        "Global routing failed. Number of unrouted nets: {}",
                        unrouted_nets.len()
                    );
                    todo!("Global routing failed.");
                }
                Ok(routes) => routes,
            }
        };

        let _airwire_router = AirWireRouter::new(0, 20, true);

        let simple_maze_router = SimpleMazeRouter::new(
            wire_width / 2,
            min_spacing,
            wire_weights,
            routing_layers.clone(),
            via_layers.clone(),
        );

        // let router = SimpleLineSearchRouter::new(wire_width / 2, min_spacing, wire_weights);

        let routing_result = {
            let mut detail_routing_problem =
                SimpleRoutingProblem::new(&self.chip, top_cell.clone())
                    .with_routing_guides(global_routes);

            // Tell the route which nets to route.
            {
                // Since the detail routing is not that advanced yet, skip high-fanout nets.
                let max_fanout = 100;
                log::warn!("Route nets with up to {} terminals.", max_fanout);
                let nets = self.get_low_fanout_nets(max_fanout);
                log::info!("Number of nets selected for routing: {}", nets.len());
                detail_routing_problem.nets.extend(nets);
            }

            simple_maze_router.route(detail_routing_problem)
        };

        if let Err((_result, unrouted_nets)) = &routing_result {
            log::error!("Detail routing failed.");
            log::error!("Number of unrouted nets: {}", unrouted_nets.len())
        } else {
            log::info!("Routed all nets.")
        }

        // Draw the routes to the layout.
        match routing_result {
            Ok(routes) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
            Err((routes, _unrouted_nets)) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
        }

        // let routing_result = router.route_all_nets(
        //     &mut self.chip,
        //     top_cell,
        //     &routing_layers,
        //     &via_layers,
        // );
        //
        // if let Err(unrouted_nets) = &routing_result {
        //     log::error!("Number of unrouted nets: {}", unrouted_nets.len());
        //     log::info!("Draw unrouted nets.");
        //     let airwire_result = airwire_router.route_nets(
        //         &mut self.chip,
        //         top_cell,
        //         &routing_layers,
        //         &via_layers,
        //         unrouted_nets,
        //     );
        //     if airwire_result.is_err() {
        //         // That should not happen.
        //         log::error!("Some unrouted nets could not be displayed with airwires.");
        //     }
        // } else {
        //     log::info!("Routed all nets.")
        // }
    }

    pub fn route_with_mycelium2(&mut self) {
        let top_cell = self.top_cell.clone().expect("No top cell defined.");

        // Make sure all nets have names.
        self.chip.create_net_names_in_circuit(&top_cell, "_");

        let routing_layers = self.get_routing_layers();
        let via_layers = self.get_via_layers();

        // Get design rules from LEF.
        let design_rules = LEFDesignRuleAdapter::new(&self.tech_lef, &self.chip);

        // Extract via definitions from the layout and the LEF file.
        let via_definitions = {
            log::info!("Extract via definitions from LEF.");
            let via_definitions = mycelium_router::extract_via_defs_from_lef(
                &self.chip,
                &self.tech_lef,
                &design_rules,
            );
            let via_definitions = match via_definitions {
                Ok(d) => d,
                Err(d) => {
                    log::warn!("Some via definitions where not extracted.");
                    d
                }
            };
            log::info!("Number of vias found: {}", via_definitions.len());
            via_definitions
        };

        let drc_engine =
            mycelium_router::mycelium_detail_route_v2::SimpleDrcEngine::new(&design_rules);

        let cell_outlines = self.get_cell_outlines();

        let chip_bbox = self
            .chip
            .bounding_box(&top_cell)
            .expect("Layout has no bounding box. Check if the layout is empty.")
            // Enlarge to cover all routing guides. That's a hack: TODO: Take bounding box of routing guides.
            .sized_isotropic(self.chip.dbu() * 10);

        // Load routing pitches in from design rules.
        let routing_pitches = routing_layers
            .iter()
            .map(|l| {
                design_rules
                    .default_pitch_preferred_direction(l)
                    .expect("missing default routing pitch for a layer")
            })
            .collect();

        // Construct routing tracks.
        let routing_tracks = create_routing_tracks(&design_rules, chip_bbox);

        // Pin access analysis
        let pin_access_oracle = {
            let builder = PinAccessOracleBuilder::new(
                &self.chip,
                top_cell.clone(),
                &design_rules,
                &drc_engine,
                Box::new(|cell| cell_outlines.get(cell).cloned()),
                via_definitions.clone(),
                routing_tracks.clone(),
            );

            builder.build()
        };

        // Get wire costs based on the routing direction.
        let mut wire_weights: Vec<(i32, i32)> = routing_layers
            .iter()
            .map(|layer| {
                let direction = design_rules
                    .preferred_routing_direction(layer)
                    .unwrap_or(db::Orientation2D::Horizontal);
                match direction {
                    Orientation2D::Horizontal => (1, 100),
                    Orientation2D::Vertical => (100, 1),
                }
            })
            .collect();

        // Avoid lowest layer.
        wire_weights[0].0 *= 100;
        wire_weights[0].1 *= 100;
        //// For debugging, try to use metal5.
        //wire_weights[1].0 *= 100;
        //wire_weights[1].1 *= 100;
        //wire_weights[2].0 *= 100;
        //wire_weights[2].1 *= 100;
        //wire_weights[3].0 *= 100;
        //wire_weights[3].1 *= 100;

        let via_weight = 1000;

        assert!(
            wire_weights.len() <= routing_layers.len(),
            "Try to use more routing layers than specified in the technology."
        );

        let global_router = SimpleGlobalRouter::new(
            routing_pitches,
            wire_weights.clone(),
            via_weight,
            routing_layers.clone(),
            via_layers.clone(),
        );

        // Get the nets which should be routed.
        let nets = {
            // Since the detail routing is not that advanced yet, skip high-fanout nets.
            let max_fanout = 100;
            log::warn!("Route nets with up to {} terminals.", max_fanout);
            let mut nets = self.get_low_fanout_nets(max_fanout);

            // Skip LOW and HIGH net.
            nets.retain(|net| !self.chip.is_constant_net(net));
            log::info!("Number of nets: {}", nets.len());
            // let nets: Vec<_> = nets
            //     .into_iter()
            //     // For debugging route only a few nets.
            //     .take(3000)
            //     .collect();

            //let net_names = vec![
            //    //"attocore_1_.registerFile_1_._1413_",
            //    // "_pad_net_clk_i_buf.156",
            //    // "pad_net_sub_0_pad",
            //    "pad_net_a_op_0_pad",
            //];
            //let nets: Vec<_> = net_names
            //    .iter()
            //    .filter_map(|n| self.chip.net_by_name(&top_cell, n))
            //    .chain(nets.into_iter().take(0))
            //    .collect();

            nets
        };

        // Global routing
        let global_routes = {
            let global_routing_problem = {
                let top_cell = self.top_cell.clone().unwrap();

                let mut routing_problem: SimpleRoutingProblem<_, ()> =
                    SimpleRoutingProblem::new(&self.chip, top_cell);
                routing_problem.nets = nets.clone();

                routing_problem
            };

            let routes = global_router
                .route_with_pin_access_oracle(&global_routing_problem, &pin_access_oracle);

            match routes {
                Err(unrouted_nets) => {
                    log::error!(
                        "Global routing failed. Number of unrouted nets: {}",
                        unrouted_nets.len()
                    );
                    todo!("Global routing failed.");
                }
                Ok(routes) => routes,
            }
        };

        let _airwire_router = AirWireRouter::new(0, 20, true);

        let detail_router = mycelium_router::mycelium_detail_route_v2::MyceliumDR2::new(
            &design_rules,
            &drc_engine,
            via_definitions,
            &cell_outlines,
        );

        let routing_result = {
            let mut detail_routing_problem =
                SimpleRoutingProblem::new(&self.chip, top_cell.clone())
                    .with_routing_guides(global_routes);

            detail_routing_problem.boundary = Some(chip_bbox.into());

            // Tell the router which nets to route.
            {
                log::info!("Number of nets selected for routing: {}", nets.len());
                detail_routing_problem.nets.extend(nets);
            }

            detail_router.route_with_pin_access_oracle(detail_routing_problem, &pin_access_oracle)
        };

        if let Err(err) = &routing_result {
            log::error!("Detail routing failed: {}", err);
            match err {
                RoutingError::RoutingFailed((_result, unrouted_nets)) => {
                    log::error!("Number of unrouted nets: {}", unrouted_nets.len())
                }
                _ => {}
            }
        } else {
            log::info!("Routed all nets.")
        }

        // Draw the routes to the layout.
        match routing_result {
            Ok(routes) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
            Err(err) => match err {
                RoutingError::RoutingFailed((routes, _unrouted_nets)) => {
                    log::warn!("Draw partial routing result to layout.");
                    let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                    if draw_result.is_err() {
                        log::error!("Failed to draw routes to layout.");
                    }
                }
                _ => {}
            },
        }

        // let routing_result = router.route_all_nets(
        //     &mut self.chip,
        //     top_cell,
        //     &routing_layers,
        //     &via_layers,
        // );
        //
        // if let Err(unrouted_nets) = &routing_result {
        //     log::error!("Number of unrouted nets: {}", unrouted_nets.len());
        //     log::info!("Draw unrouted nets.");
        //     let airwire_result = airwire_router.route_nets(
        //         &mut self.chip,
        //         top_cell,
        //         &routing_layers,
        //         &via_layers,
        //         unrouted_nets,
        //     );
        //     if airwire_result.is_err() {
        //         // That should not happen.
        //         log::error!("Some unrouted nets could not be displayed with airwires.");
        //     }
        // } else {
        //     log::info!("Routed all nets.")
        // }
    }

    /// Route the design using TritonRoute(-WXL).
    pub fn route_with_triton(&mut self) {
        let top_cell = self.top_cell.clone().expect("No top cell defined.");

        // Set routing tracks.
        let triton = {
            // let mut triton: libreda_triton_route::TritonRoute<Chip> = libreda_triton_route::TritonRoute::new(
            //     "/opt/TritonRoute".into(), technology_lef_path.into(),
            //     self.outline_layer.expect("No outline layer specified."),
            // );

            let mut triton: libreda_triton_route::TritonRoute<C> =
                libreda_triton_route::TritonRoute::new_wxl(
                    "/opt/TritonRoute-WXL".into(),
                    self.tech_lef_path.clone(),
                    self.outline_layer
                        .clone()
                        .expect("No outline layer specified."),
                );

            // Set routing tracks.
            {
                let routing_layers = self.get_routing_layers();

                // Get design rules from LEF.
                let design_rules = LEFDesignRuleAdapter::new(&self.tech_lef, &self.chip);

                let die_area: db::Rect<_> = self
                    .core_area()
                    .try_bounding_box()
                    .expect("Core shape must have a defined bounding box.");
                let def = triton.template_def();

                // Add TRACKS statements to the DEF structure.
                for routing_layer in &routing_layers {
                    let layer_name: String =
                        self.chip.layer_info(routing_layer).name.unwrap().into();

                    let pitch = design_rules
                        .default_pitch_preferred_direction(routing_layer)
                        .expect("routing pitch is not defined for all layers");
                    let orientation = design_rules
                        .preferred_routing_direction(routing_layer)
                        .expect("preferred routing direction is not defined for all layers");

                    // Quick and dirty fix, otherwise TritonRoute does not find
                    // access points for all pins.
                    let pitch = if layer_name == "metal1" || layer_name == "metal2" {
                        100
                    } else {
                        pitch
                    };

                    let is_horizontal = orientation.is_horizontal();

                    let start = match is_horizontal {
                        true => die_area.lower_left().y,
                        false => die_area.lower_left().x,
                    };

                    let num_tracks = match is_horizontal {
                        true => (die_area.height() / pitch + 1) as u32,
                        false => (die_area.width() / pitch + 1) as u32,
                    };

                    let tracks = Tracks {
                        is_horizontal,
                        start,
                        num_tracks,
                        step: pitch,
                        mask: None,
                        layers: vec![layer_name],
                    };

                    def.tracks.push(tracks.clone());
                }
            }

            triton
        };

        if !triton.check_configuration() {
            let msg = "Something is wrong with the configuration of TritonRouter.";
            log::error!("{}", msg);
            panic!("{}", msg);
        }

        // Detail routing.
        {
            // FIXME: Hack remove pad cells because they have no defined outline and are not specified in the LEF file.
            self.chip
                .remove_cell(&self.chip.cell_by_name("dummy_pad_output").unwrap());
            self.chip
                .remove_cell(&self.chip.cell_by_name("dummy_pad_input").unwrap());
            self.chip
                .remove_cell(&self.chip.cell_by_name("TIEHIGH").unwrap());
            self.chip
                .remove_cell(&self.chip.cell_by_name("TIELOW").unwrap());
            //
            // Remove all the nets which are not used after removing the above cells.
            self.chip.purge_nets_in_circuit(&top_cell);
        }

        // Make sure all nets have names. This is required for DEF export and import.
        self.chip.create_net_names_in_circuit(&top_cell, "_");

        let routing_result = {
            let mut detail_routing_problem: SimpleRoutingProblem<_, SimpleGlobalRoute<C>> =
                SimpleRoutingProblem::new(&self.chip, top_cell.clone());

            // Tell the route which nets to route. (All nets in this case).
            detail_routing_problem
                .nets
                .extend(self.chip.each_internal_net(&top_cell));

            triton.route(detail_routing_problem)
        };

        if let Err((_result, unrouted_nets)) = &routing_result {
            log::warn!("Detail routing did not complete for all nets (mainly because detail routing is not fully implemented yet).");
            log::warn!("Number of unrouted nets: {}", unrouted_nets.len())
        } else {
            log::info!("Routed all nets.")
        }

        // Draw the routes to the layout.
        match routing_result {
            Ok(routes) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
            Err((routes, _unrouted_nets)) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
        }
    }

    /// Create output path parent directories if does not exist
    pub fn create_parent_dirs(&self, out_path: &str) {
        let path = std::path::Path::new(out_path);
        let prefix = path.parent().expect("failed to get parent of path");
        if !path.exists() {
            log::debug!("Create directory: {}", prefix.display());
            std::fs::create_dir_all(prefix).expect("failed to create directory");
        }
    }

    /// Store the layout into a OASIS file.
    pub fn store_layout(&self, stream_out_path: &str) {
        info!("Stream out: {}", stream_out_path);
        let oasis_writer = libreda_oasis::OASISStreamWriter::default();
        debug!("Number of cells in layout: {}", self.chip.num_cells());

        // TODO: Already open the file to detect early if it fails.
        self.create_parent_dirs(stream_out_path);
        let stream_out_file = &mut fs::File::create(stream_out_path).unwrap();
        let err = oasis_writer.write_layout(&mut BufWriter::new(stream_out_file), &self.chip);
        if let Err(err) = err {
            error!("Error: {}", err);
        }
    }

    /// Dump the netlist to a verilog file.
    pub fn store_netlist(&self, path: &str) {
        let netlist_writer = StructuralVerilogWriter::new();
        log::info!("Write verilog netlist: {}", path);
        self.create_parent_dirs(path);
        let mut netlist_output_file = fs::File::create(path).unwrap();
        netlist_writer
            .write_netlist(&mut netlist_output_file, &self.chip)
            .expect("Failed to write netlist.");
    }

    /// Get a list of nets in the current top-cell.
    pub fn get_low_fanout_nets(&self, max_fanout: usize) -> Vec<C::NetId> {
        // Sort nets. Nets with highest fanout come first.
        self.top_cell_ref()
            .each_net()
            .filter(|net| net.num_terminals() <= max_fanout)
            .map(|net| net.id())
            .collect()
    }

    /// Print a list of nets with highest number of pins.
    pub fn report_high_fanout_nets(&self, max_num_nets: usize) {
        log::info!("Report high-fanout nets.");
        // Sort nets. Nets with highest fanout come first.
        let mut nets: Vec<_> = self.top_cell_ref().each_net().collect();
        nets.sort_by_key(|net| net.num_terminals());
        nets.reverse();

        // Print the first few nets.
        let nets = nets.into_iter().take(max_num_nets);
        for net in nets {
            println!("{} terminals: {}", net.num_terminals(), net.qname(":"));
        }
    }
}
