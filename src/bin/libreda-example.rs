/*
 * Copyright (c) 2020-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Example place & route flow of the LibrEDA framework.
//! The does not target any real technology and serves demonstration purpose only.
//! Currently this code is highly experimental and subject to vivid changes.

// stdlib
use std::collections::{HashMap, HashSet};
use std::io::Write;
use std::process::exit;

// crates.io
use clap::{App, Arg};
use itertools::Itertools;
use log;
use log::Level;

use libreda_db::prelude as db;
use libreda_db::prelude::*;

use libreda_oasis::*;

use tetris_legalizer::DenseFirstLegalizerMS;

use libreda_pnr::legalize::stdcell_legalizer::legalize;

use libreda_examples_simple_flow::simple_flow::SimpleFlow;

/// Create a new cell which holds an instance of `top` and a ring
/// of pads around.
///
/// # Returns
/// Returns a tuple `(cell with pads, pad instances)`.
fn create_pads<C: L2NEdit>(chip: &mut C, top: &C::CellId) -> (C::CellId, Vec<C::CellInstId>) {
    // Create a circuit which contains also the pads.
    log::info!("Create top-level circuit with dummy pads.");
    let top_pad = chip.create_cell("top_pad".to_string().into());
    let top_inst = chip.create_cell_instance(&top_pad, &top, Some("top_inst".to_string().into()));

    // Create dummy pad circuit.
    let dummy_pad_input = chip.create_cell("dummy_pad_input".to_string().into());
    chip.create_pin(
        &dummy_pad_input,
        "Out".to_string().into(),
        db::Direction::Output,
    );
    let dummy_pad_output = chip.create_cell("dummy_pad_output".to_string().into());
    chip.create_pin(
        &dummy_pad_output,
        "In".to_string().into(),
        db::Direction::Input,
    );

    // Attach dummy pads to the pins of the top instance.
    let mut pad_instances: Vec<_> = Vec::new();
    for pin_inst in chip.each_pin_instance_vec(&top_inst) {
        let pin = chip.template_pin(&pin_inst);
        let direction = chip.pin_direction(&pin);

        // Choose the correct pad type for this pin direction.
        let pad_type = match direction {
            db::Direction::Input => &dummy_pad_input,
            db::Direction::Output => &dummy_pad_output,
            d => panic!("IO pin has unsupported direction: {:?}", d),
        };

        let pad = chip.create_cell_instance(
            &top_pad,
            pad_type,
            Some(format!("dummy_pad_{}", chip.pin_name(&pin)).into()),
        );
        let pad_net = chip.create_net(
            &top_pad,
            Some(format!("pad_net_{}", chip.pin_name(&pin)).into()),
        );
        chip.connect_pin_instance(
            &chip.each_pin_instance_vec(&pad)[0], // Connect to first pin.
            Some(pad_net.clone()),
        );
        chip.connect_pin_instance(&pin_inst, Some(pad_net));
        pad_instances.push(pad);
    }

    // chip.flatten_circuit(&top);
    chip.flatten_circuit_instance(&top_inst);

    {
        // Clean unused nets.
        log::info!("Purge unused nets.");
        let num_purged = chip.purge_nets_in_circuit(&top_pad);
        log::info!("Purged {} nets.", num_purged);
    }

    (top_pad, pad_instances)
}

fn init_log_format() {
    // Set log level with environment variables like: RUST_LOG=info
    let start_time = std::time::Instant::now();
    env_logger::builder()
        .format_timestamp(None)
        .format(move |buf, record| {
            use env_logger::fmt::Color;

            // Set colors of log level.
            let mut level_style = buf.style();
            match record.level() {
                Level::Trace => level_style.set_color(Color::Cyan),
                Level::Debug => level_style.set_color(Color::Blue),
                Level::Info => level_style.set_color(Color::Green),
                Level::Warn => level_style.set_color(Color::Yellow),
                Level::Error => level_style.set_color(Color::Red),
            };

            write!(buf, "[{:>8.2}s ", start_time.elapsed().as_secs_f64())?;
            write!(buf, "{} ", level_style.value(record.level()))?;
            if let Some(module) = record.module_path() {
                write!(buf, " {} ", module)?;
            }
            write!(buf, "] {}", record.args())?;

            writeln!(buf)
        })
        .init();
}

fn main() {
    init_log_format();

    let cmdline_args = App::new("LibrEDA example flow")
        .version("0.0.0")
        .author("Thomas Kramer <code@tkramer.ch>")
        .about("LibrEDA example flow.")
        .long_about(
            "Example executable for the LibrEDA place & route framework. \
        The does not target any real technology and serves demonstration purpose only.",
        )
        .arg(
            Arg::with_name("netlist")
                .short("n")
                .long("netlist")
                .takes_value(true)
                .required(true)
                .help("Gate-level netlist (verilog)."),
        )
        .arg(
            Arg::with_name("cell-library")
                .long("cell-library")
                .takes_value(true)
                .required(true)
                .help("Verilog library of the standard cells."),
        )
        .arg(
            Arg::with_name("technology-lef")
                .long("lef")
                .takes_value(true)
                .required(true)
                .help("Path to the technology LEF file."),
        )
        .arg(
            Arg::with_name("cell-layouts")
                .long("cell-layouts")
                .takes_value(true)
                .required(true)
                .help("Path to directory with OASIS layouts of the cells."),
        )
        .arg(
            Arg::with_name("top-cell-name")
                .long("top")
                .takes_value(true)
                .required(true)
                .help("Name of the top level cell."),
        )
        .arg(
            Arg::with_name("output")
                .long("output")
                .takes_value(true)
                .required(true)
                .help("Output file for the final layout."),
        )
        .arg(
            Arg::with_name("macro-width")
                .short("w")
                .long("width")
                .takes_value(true)
                .required(true)
                .help("Width of the macro."),
        )
        .arg(
            Arg::with_name("macro-height")
                .short("h")
                .long("height")
                .takes_value(true)
                .required(true)
                .help("Height of the macro."),
        )
        .arg(
            Arg::with_name("density")
                .long("density")
                .takes_value(true)
                .value_name("TARGET_DENSITY")
                .help("Target density for placement"),
        )
        .arg(
            Arg::with_name("place-max-iter")
                .long("place-max-iter")
                .takes_value(true)
                .value_name("PLACE_MAX_ITER")
                .help("Maximum number of iterations for placement"),
        )
        .arg(
            Arg::with_name("clock")
                .long("clock")
                .multiple(true)
                .takes_value(true)
                .required(false)
                .help("Name of clock nets."),
        )
        .arg(
            Arg::with_name("reset")
                .long("reset")
                .multiple(true)
                .takes_value(true)
                .required(false)
                .help("Name of reset nets."),
        )
        .arg(
            Arg::with_name("skip-powergrid")
                .long("skip-powergrid")
                .help("Don't create the power grid."),
        )
        .arg(
            Arg::with_name("skip-legalization")
                .long("skip-legalization")
                .help("Abort after global placement and output the result."),
        )
        .arg(
            Arg::with_name("skip-routing")
                .long("skip-routing")
                .help("Don't do the routing and output just the placed design."),
        )
        .arg(
            Arg::with_name("router")
                .long("router")
                .possible_values(&["triton", "mycelium", "mycelium2"])
                .default_value("mycelium2")
                .help("Name of the routing engine."),
        )
        .get_matches();

    // Core dimensions.
    let core_width: db::SInt = cmdline_args
        .value_of("macro-width")
        .expect("Macro width must be defined.")
        .parse()
        .expect("Macro width could not be parsed to an integer.");

    let core_height = cmdline_args
        .value_of("macro-height")
        .expect("Macro height must be defined.")
        .parse()
        .expect("Macro height could not be parsed to an integer.");

    let cells_library_path = cmdline_args
        .value_of("cell-library")
        .expect("Path to cell library verilog file must be defined.");

    let target_density: f64 = cmdline_args
        .value_of("density")
        .map(|d| d.parse().expect("Density could not be parsed to a float."))
        .unwrap_or(0.5);

    let place_max_iter: usize = cmdline_args
        .value_of("place-max-iter")
        .map(|d| {
            d.parse()
                .expect("Maximum iterations must be positive integer.")
        })
        .unwrap_or(1500);

    assert!(
        target_density > 0.0 && target_density <= 1.0,
        "placement density must be a value between 0 and 1"
    );

    let cells_layout_path = cmdline_args
        .value_of("cell-layouts")
        .expect("Path to directory with cell layouts must be defined");

    let top_cell_name = cmdline_args
        .value_of("top-cell-name")
        .expect("Top cell name must be given");
    log::info!("Top cell: {}", top_cell_name);

    // Get specified clock and reset nets.
    let clock_pins: Vec<&str> = cmdline_args
        .values_of("clock")
        .map(|iter| iter.collect())
        .unwrap_or(vec![]);
    let reset_pins: Vec<&str> = cmdline_args
        .values_of("reset")
        .map(|iter| iter.collect())
        .unwrap_or(vec![]);

    let stream_out_path = cmdline_args.value_of("output").unwrap_or("output.oas");

    let technology_lef_path = cmdline_args
        .value_of("technology-lef")
        .unwrap_or("Path to technology LEF must be specified.");

    let top_netlist_path = cmdline_args
        .value_of("netlist")
        .expect("Path to netlist file must be given.");
    log::info!("Top netlist: {}", top_netlist_path);

    let skip_powergrid = cmdline_args.is_present("skip-powergrid");
    let skip_legalization = cmdline_args.is_present("skip-legalization");
    let skip_routing = skip_legalization || cmdline_args.is_present("skip-routing");

    // Setup the flow.

    let mut flow: SimpleFlow<db::DBPerf<Chip>> = SimpleFlow::new();
    flow.placement_target_density = target_density;
    flow.placement_max_iter = place_max_iter;

    flow.core_area = Some(db::Rect::new((0, 0), (core_width, core_height)).into());

    // Read LEF.
    let result = flow.read_technology_lef(technology_lef_path);
    if let Err(err) = result {
        log::error!("Failed to parse LEF: {}", err);
        exit(1);
    }

    // Print information about the routing stack.
    flow.report_routing_stack();

    // Read netlist library.
    flow.read_cell_netlist_library(cells_library_path);

    // Read design netlist.
    flow.read_design_netlist(top_netlist_path);

    // Get layer which holds cell outlines.
    flow.outline_layer = Some(flow.chip.find_or_create_layer(63, 0));

    let top = flow
        .select_top_cell(top_cell_name)
        .expect("Failed to select top cell.");

    // Deep-flatten the top with pads such that only leaf cells are inside the top cell.
    flow.deep_flatten_design(&top);

    // // Output flattened netlist for debugging.
    // flow.store_netlist("/tmp/flattened.v");

    // Create pad instances.
    let (top_pad, pad_instances) = create_pads(&mut flow.chip, &top);
    flow.top_cell = Some(top_pad.clone());

    let clock_pins: Vec<_> = clock_pins
        .into_iter()
        .map(|n| format!("pad_net_{}", n))
        .collect();
    let reset_pins: Vec<_> = reset_pins
        .into_iter()
        .map(|n| format!("pad_net_{}", n))
        .collect();

    // // Output flattened netlist for debugging.
    // flow.store_netlist("/tmp/flattened_with_pads.v");

    // Register clock and reset pins.
    for clk in clock_pins {
        if flow
            .chip
            .net_by_name(&flow.top_cell.unwrap(), clk.as_str())
            .is_none()
        {
            log::error!("Clock net not found: {}", clk);
            exit(1);
        }
        log::info!("Clock net: {}", clk);
        flow.add_clock_net(clk);
    }
    for rst in reset_pins {
        if flow
            .chip
            .net_by_name(&flow.top_cell.unwrap(), rst.as_str())
            .is_none()
        {
            log::error!("Reset net not found: {}", rst);
            exit(1);
        }
        log::info!("Reset net: {}", rst);
        flow.add_reset_net(rst);
    }

    // Draw core area for illustration.
    if let Some(l_outline) = flow.outline_layer.clone() {
        flow.chip.insert_shape(
            flow.top_cell.as_ref().unwrap(),
            &l_outline,
            flow.core_area().into(),
        );
    }

    // Sanity checks.
    flow.validate_netlist_check_drivers();

    {
        // Report used cells.
        log::info!("Top cell: {}", flow.chip.cell_name(&top_pad));
        log::info!(
            "Number of cell instances: {}",
            flow.chip.num_child_instances(&top_pad)
        );
        let used_std_cells = flow.get_used_std_cell_names();
        log::info!("Used cells: {}", used_std_cells.iter().join(", "));
    }

    // Load cell layouts from disk.
    flow.load_cell_layout_library(cells_layout_path);
    flow.detect_pins_in_layout();

    // { // Add a big dummy macro block for testing the placer.
    //     let dummy_macro = flow.chip.create_cell("DUMMY_MACRO".to_string());
    //
    //     flow.chip.insert_shape(&dummy_macro, &l_outline,
    //                       db::Rect::new((0,0), (20000, 50000)).into()
    //     );
    //
    //     let inst = flow.chip.create_cell_instance(&top_pad, &dummy_macro, None);
    //     flow.chip.set_transform(&inst, db::SimpleTransform::translate((5000, 5000)));
    //     let inst1 = flow.chip.create_cell_instance(&top_pad, &dummy_macro, None);
    //     flow.chip.set_transform(&inst1, db::SimpleTransform::translate((1000, 10000)));
    //     let inst2 = flow.chip.create_cell_instance(&top_pad, &dummy_macro, None);
    //     flow.chip.set_transform(&inst2, db::SimpleTransform::translate((20000, 20000)));
    // }

    {
        // Placement.
        // Create legalizer.

        let cell_dimensions = flow.get_cell_outlines();

        // Find height of standard cells. Heuristic: Take the one which occurs most often.
        let row_height = {
            let leaf_cells = flow.chip.each_leaf_cell();
            let heights =
                leaf_cells.flat_map(|id| cell_dimensions.get(&id).map(|dim| (id, dim.height())));
            // Heuristic: Take the height which occurs most often.
            {
                let mut histogram = HashMap::new();
                for (cell_id, height) in heights {
                    let num_instances = flow.chip.num_cell_references(&cell_id);
                    *histogram.entry(height).or_insert(0) += num_instances;
                }

                let most_frequent_height = histogram
                    .iter()
                    .max_by_key(|(_height, &count)| count)
                    .map(|(height, _count)| *height)
                    .expect("Could not find the height of the standard-cells.");
                log::info!("Detected standard-cell height: {}", most_frequent_height);
                most_frequent_height
            }
        };
        let x_grid = 10;
        let legalizer = DenseFirstLegalizerMS::new(row_height, x_grid);

        flow.validate_layout_density();

        // Place the cells.
        let top_cell = flow.chip.create_cell("TOP".into());

        // Create initial positions for the pads.
        let initial_positions = flow.get_pad_positions(&pad_instances, (core_width, core_height));

        let core_area = flow.core_area();

        // Create cell instances in the layout and place them at a default location.
        {
            // Set the initial location of the unplaced library cells.
            let default_location =
                db::SimpleTransform::translate(core_area.try_bounding_box().unwrap().center());
            for inst in flow.chip.each_cell_instance_vec(&top_pad) {
                let tf = initial_positions
                    .get(&inst)
                    .map(|&p| db::SimpleTransform::translate(p))
                    .unwrap_or(default_location.clone());
                flow.chip.set_transform(&inst, tf);
            }
        }

        let fixed_instances: HashSet<_> = initial_positions.keys().cloned().collect();

        // Global placement.
        flow.place_global(&top_pad, &fixed_instances, true);

        // Create buffer trees on high-fanout nets (for demonstration only).
        let _buffer_tree_nets = flow.rebuffer_high_fanout_nets();

        // Global placement with buffer trees.
        flow.place_global(&top_pad, &fixed_instances, false);

        // Replace constant `0` and `1` nets with tie cells.
        flow.insert_tie_cells();

        {
            // Put net names of pads for debugging.
            let label_layer = flow.chip.find_or_create_layer(200, 0);
            for pad in &pad_instances {
                let pos = flow.chip.get_transform(pad);
                // Get the pin of the pad, blindly assuming there is one.
                let pad_pin_inst = flow.chip.each_pin_instance_vec(pad)[0];
                // if let Some(net) = pad.net_for_pin(0) {
                if let Some(net) = flow.chip.net_of_pin_instance(&pad_pin_inst) {
                    if let Some(name) = flow.chip.net_name(&net) {
                        let text = db::Text::new(name.to_string(), pos.displacement.into());
                        flow.chip.insert_shape(&top_cell, &label_layer, text.into());
                    }
                }
            }
        }

        // ** LEGALIZATION **

        if !skip_legalization {
            // Find cell instances that can be moved by the legalizer.
            let movable_instances: HashSet<_> = flow
                .chip
                .each_cell_instance(&top_pad)
                .filter(|c| !fixed_instances.contains(&c))
                .collect();

            // Legalize buffers and tie-cells.
            legalize(
                &legalizer,
                &mut flow.chip,
                &top_pad,
                &core_area,
                &cell_dimensions,
                &movable_instances,
            );
        }

        // Do sanity checks on the netlist.
        flow.validate_netlist_check_drivers();

        if skip_powergrid {
            log::warn!("Skip power grid.");
        } else {
            // Create the power grid.
            flow.draw_power_grid(&core_area, row_height as db::Coord);
        }
    }

    // Report on high-fanout nets.
    flow.report_high_fanout_nets(20);

    // Route
    if !skip_routing {
        match cmdline_args.value_of("router") {
            Some("triton") => flow.route_with_triton(),
            Some("mycelium") => {
                flow.route_with_mycelium();
            }
            Some("mycelium2") => {
                flow.route_with_mycelium2();
            }
            Some(x) => {
                log::error!("Routing engine not found: '{}'", x);
            }
            None => {
                log::error!("No routing engine specified.");
            }
        }
    }

    // Store the layout.
    flow.store_layout(stream_out_path);

    report_db_performance(&flow.chip, log::Level::Debug);
}

/// Print statistics on time spent in functions of
/// the data-base API.
fn report_db_performance<C>(chip: &DBPerf<C>, log_level: log::Level) {
    let mut stats = chip.get_stats_all();

    // Sort by time spent.
    stats.sort_by_key(|(_name, stat)| stat.total_time);

    let mut num_unused_functions = 0;
    for (function_name, stat) in stats {
        // Skip unused functions.
        if stat.num_calls == 0 {
            num_unused_functions += 1;
            continue;
        };

        log::log!(
            log_level,
            "{}: {} calls, {:?}",
            function_name,
            stat.num_calls,
            stat.total_time,
        );
    }
    log::log!(
        log_level,
        "Number of unused DB API functions: {}",
        num_unused_functions
    );
}
