#!/bin/bash

# Run a simple-stupid example place and route flow.
# Inputs are: Directory to the cell layouts, a verilog file with the cell interfaces
# a LEF file with the cell library, a synthesized gate-level netlist the name of the top-level cell
# and core dimensions.
# 
# Use the --help argument to display documentation on the arguments.
#

ARGS="${@:1}"

mkdir test/output

RUST_LOG=info cargo run \
    --release \
    --bin libreda-example -- \
    --cell-layouts ./data/oas/ \
    --cell-library ./data/gscl45nm_interfaces.v \
    --lef ./data/gscl45nm.lef \
     --netlist ./data/Murax_8kB_freepdk45_nl.v \
     --top Murax \
     --reset io_asyncReset \
     --clock io_mainClk \
    --height 1500000 --width 1500000 \
    --output ./output/murax_8kB.oas \
    $ARGS

