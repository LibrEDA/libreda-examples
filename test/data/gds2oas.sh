#!/bin/sh

set -e

for f in gds/*; do
    echo $f
    strm2oas "$f" oas/$(basename "$f").oas
done
